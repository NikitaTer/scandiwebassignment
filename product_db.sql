-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Май 19 2020 г., 14:42
-- Версия сервера: 10.4.11-MariaDB
-- Версия PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `product_db`
--

-- --------------------------------------------------------

--
-- Структура таблицы `book`
--

CREATE TABLE `book` (
  `pr_sku` char(10) NOT NULL,
  `weight` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `book`
--

INSERT INTO `book` (`pr_sku`, `weight`) VALUES
('1', 0.5);

-- --------------------------------------------------------

--
-- Структура таблицы `diskdvd`
--

CREATE TABLE `diskdvd` (
  `pr_sku` char(10) NOT NULL,
  `sizemb` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `diskdvd`
--

INSERT INTO `diskdvd` (`pr_sku`, `sizemb`) VALUES
('2', 450),
('4', 758);

-- --------------------------------------------------------

--
-- Структура таблицы `furniture`
--

CREATE TABLE `furniture` (
  `pr_sku` char(10) NOT NULL,
  `height` float DEFAULT NULL,
  `width` float DEFAULT NULL,
  `length` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `furniture`
--

INSERT INTO `furniture` (`pr_sku`, `height`, `width`, `length`) VALUES
('3', 1.5, 5.2, 2.7);

-- --------------------------------------------------------

--
-- Структура таблицы `product`
--

CREATE TABLE `product` (
  `sku` char(10) NOT NULL,
  `name` char(25) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `product`
--

INSERT INTO `product` (`sku`, `name`, `price`, `type_id`) VALUES
('1', 'Creek', 5.22, 1),
('2', 'SonicDVD', 9.95, 2),
('3', 'Sofa', 35.22, 3),
('4', 'Hima', 5, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `pr_type`
--

CREATE TABLE `pr_type` (
  `id` int(11) NOT NULL,
  `name` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `pr_type`
--

INSERT INTO `pr_type` (`id`, `name`) VALUES
(1, 'Book'),
(2, 'DiskDVD'),
(3, 'Furniture');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`pr_sku`);

--
-- Индексы таблицы `diskdvd`
--
ALTER TABLE `diskdvd`
  ADD PRIMARY KEY (`pr_sku`);

--
-- Индексы таблицы `furniture`
--
ALTER TABLE `furniture`
  ADD PRIMARY KEY (`pr_sku`);

--
-- Индексы таблицы `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`sku`),
  ADD KEY `type_id` (`type_id`);

--
-- Индексы таблицы `pr_type`
--
ALTER TABLE `pr_type`
  ADD PRIMARY KEY (`id`);

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `book`
--
ALTER TABLE `book`
  ADD CONSTRAINT `book_ibfk_1` FOREIGN KEY (`pr_sku`) REFERENCES `product` (`sku`);

--
-- Ограничения внешнего ключа таблицы `diskdvd`
--
ALTER TABLE `diskdvd`
  ADD CONSTRAINT `diskdvd_ibfk_1` FOREIGN KEY (`pr_sku`) REFERENCES `product` (`sku`);

--
-- Ограничения внешнего ключа таблицы `furniture`
--
ALTER TABLE `furniture`
  ADD CONSTRAINT `furniture_ibfk_1` FOREIGN KEY (`pr_sku`) REFERENCES `product` (`sku`);

--
-- Ограничения внешнего ключа таблицы `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `pr_type` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
